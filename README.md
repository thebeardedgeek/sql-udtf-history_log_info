# SQL UDTF History Log Info #

A SQL user defined table function to return information from the history log.

This UDTF is defined like the same function provided by IBM in V7R3 TR1 and V7R2 TR5. However, this will work in V7R1 as well.

### What is this repository for? ###

* This SQL function will return information about the messages in the system history log.

* The following parameters can be specified:

Parameter                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Starting time (optional)           | timestamp                     | Specifies the starting time for messages to be returned. Default is current date at midnight
Ending time (optional)             | timestamp                     | Specifies the ending time for messages to be returned. Default is current time
Message ID list (optional)         | varchar(1599)                 | Specifies the message ID's to be returned. The list may contain up to 200 entries, separated by blank or comma. Generic message ID's can be used. Default all message ID's are returned.
Message type list (optional)       | varchar(54)                   | Specifies the message types to be returned. The list may contain up to 9 entries, separated by blank or comma. Default all message types are returned. Valid values in the list are *COMP, *COPY, *DIAG, *ESCAPE, *INFO, *INQ, *NOTIFY, *RPY, *RQS.
Job list (optional)                | varchar(144)                  | Specifies the jobs to be returned. The list may contain up to 5 qualified job entries, separated by blank or comma. Each job entry must be on the form [number/[user/]]job. Default is all jobs are returned.

* The following attributes are returned:

Attribute                          | Data type                     | Description
-----------------------------------|:------------------------------|:------------------------------------------
Ordinal Position                   | integer                       | A unique number for each row
Message Id                         | varchar(7)                    | The message ID for this message
Message Type                       | varchar(13)                   | Type of message
Message Subtype                    | varchar(22)                   | Subtype of message
Severity                           | smallint                      | The severity assigned to the message
Message Timestamp                  | timestamp                     | The timestamp when the message was sent
From User                          | varchar(10)                   | The current user of the job when the message was sent
From Job                           | varchar(28)                   | The qualified job name when the message was sent
From Program                       | varchar(10)                   | The program that sent the message (always null!)
Message Library                    | varchar(10)                   | The name of the library containing the message file
Message File                       | varchar(10)                   | The message file containing the message
Message Tokens                     | varchar(4096) for bit data    | The message token string
Message Text                       | vargraphic(1024) ccsid 1200   | The first level text of the message including tokens, or the impromptu message text
Message Second Level Text          | vargraphic(4096) ccsid 1200   | The second level text of the message including tokens

Please note that attribute From Program is always returned as null! This information is not provided in the API to open history log messages. A RFE has been created for this, but whether the RFE will be accepted is questionable.

Also note that filtering the information returned from this SQL function by specifying message ID, type and/or job can reduce the runtime of the function considerably, especially with large timespans.
This is because formatting the internal data from the history log into the external format returned is CPU intensive, e.g. message data and timestamps.
The history log can contain a huge amount of data, so the smaller amount of data to format, the quicker the SQL function will return. When you only use the WHERE clause to filter the result, the SQL function has to grab and format all the data to the internal table in SQL before the WHERE clause is applied.


### How do I get set up? ###

* Clone this git repository to a local directory in the IFS, e.g. in your home directory.
* Compile the source using the following CL commands (objects will be placed in the QGPL library):

```

CRTSQLRPGI OBJ((QGPL/HSTLOGINF) SRCSTMF('hstloginf.rpgle') OBJTYPE(*MODULE)
CRTSRVPGM  SRVPGM(QGPL/HSTLOGINF) EXPORT(*ALL) TEXT('History_Log_Info UDTF')
RUNSQLSTM  SRCSTMF('udtf_History_Log_Info.sql') DFTRDBCOL(QGPL)

```
* Call the SQL function like the following
```
select * from table(qgpl.history_log_info( timestamp('2016-11-01-07.00.00.000000'), timestamp('2016-11-01-07.30.00.000000') )) x
```
* or with only starting timestamp (ending timestamp will be current timestamp):
```
select * from table(qgpl.history_log_info( timestamp('2016-11-01-07.00.00.000000') )) x
```
* or with neither starting nor ending timestamp (starting timestamp will be current date from midnight and ending timestamp will be current timestamp):
```
select * from table(qgpl.history_log_info( )) x
```

More advanced selection:

* To select only specific message ID's, call it like this:
```
-- Show todays job start (CPF1124) and job completion (CPF1164) messages:
select * from table(qgpl.history_log_info( timestamp(current_date), current_timestamp, 'cpf1124,cpf1164' )) x
```
* To select only specific message types, call it like this:
```
-- Show last two days informational messages:
select * from table(qgpl.history_log_info( timestamp(current_date - 1 day), current_timestamp, '', '*info' )) x
```
* To select only specific jobs, call it like this:
```
-- Show last seven days system cleanup messages:
select message_timestamp
     , from_job
     , message_text
  from table(qgpl.history_log_info(
               timestamp_iso(current_date - 7 days),
               current_timestamp,
               'CPI1E00',                         -- generic message! All CPI1Exx messages will be selected.
               '',
               'QCLNUSRMSG,QCLNSYSPRT,QCLNSYSMSG,QCLNUSRPGM,QCLNSYSLOG'
               )
            ) x;
```


### Documentation ###

[Open List of History Log Messages (QMHOLHST) API](http://www.ibm.com/support/knowledgecenter/ssw_ibm_i_71/apis/qmholhst.htm)

[DB2 for i - Services - QSYS2.HISTORY_LOG_INFO()](https://www.ibm.com/developerworks/community/wikis/home?lang=en#!/wiki/IBM%20i%20Technology%20Updates/page/QSYS2.HISTORY_LOG_INFO%20UDTF)